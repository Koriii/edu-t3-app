import { zodResolver } from "@hookform/resolvers/zod";
import { motion, useAnimationControls } from "framer-motion";
import router from "next/router";
import { useState } from "react";
import { useForm } from "react-hook-form";
import Image from "next/image";
import DiscordLogo from "../../../static/discord-mark-white.svg";
import { Button } from "../../../components/atoms";
import { RegisterWrapper } from "../../../components/molecules";
import BackButton from "../../../components/molecules/BackButton";
import FormInput from "../../../components/molecules/FormInput";
import { tabTypes } from "../../../components/molecules/RegisterWrapper";
import { createUserSchema, ICreateUser } from "../../../server/common/validation/auth";
import { trpc } from "../../../utils/trpc";
import { signIn, useSession } from "next-auth/react";
import Link from "next/link";

// unite with signin
// const getRandomArbitrary = (min: number, max: number) => {
//     return Math.round(Math.random() * (max - min) + min);
// };

export default function RegisterPage() {
    const [selectedTab, setSelectedTab] = useState<tabTypes>("credentials");
    const controls = useAnimationControls();
    const { status } = useSession();

    const {
        register,
        handleSubmit,
        formState: { errors, isValid, isSubmitting },
        getValues,
    } = useForm<ICreateUser>({ resolver: zodResolver(createUserSchema) });

    const { mutate, error, isError, variables } = trpc.useMutation(["user.register-user"], {
        onError: (err) => {
            console.log(err);
        },
        onSuccess: () => {
            router.push("/auth/signin");
        },
    });

    // const [hoveredTimes, setHoveredTimes] = useState(0);
    // const [color, setColor] = useState<colorType>("default");

    // TODO: play with error handling
    // const onHover = () => {
    //     trigger();
    //     setHoveredTimes((prev) => prev + 1);

    //     if (!isValid) {
    //         const randomColorIndex = getRandomArbitrary(0, colorArray.length - 1) ?? 0;
    //         setColor(colorArray[randomColorIndex] as colorType);
    //         controls.start({
    //             x: `calc(${getRandomArbitrary(-45, 25)}vw - 50%)`,
    //             y: `calc(${getRandomArbitrary(-50, 30)}vh - 50%)`,
    //         });
    //     }
    // };
    // const onSubmit:  = data => console.log(data);

    const onSubmit = (data: ICreateUser) => {
        if (isValid) {
            mutate(data);
        }
    };

    return (
        <>
            <div className="flex flex-col justify-center items-center h-screen">
                <RegisterWrapper selectedTab={selectedTab} setSelectedTab={(item) => setSelectedTab(item)}>
                    {selectedTab === "credentials" ? (
                        <>
                            <form className="flex flex-col w-80 md:w-96" onSubmit={handleSubmit(onSubmit)}>
                                <FormInput
                                    id="email"
                                    // type="email"
                                    placeholder="This does nothing"
                                    {...register("email")}
                                    valid={
                                        !(errors.email?.message || (isError && getValues().email === variables?.email))
                                    }
                                    errorMessage={errors.email?.message?.toString() ?? error?.message}
                                    disabled={isSubmitting}
                                >
                                    Email
                                </FormInput>
                                <FormInput
                                    id="password"
                                    type="password"
                                    placeholder="Useless password"
                                    {...register("password")}
                                    valid={!errors.password?.message}
                                    errorMessage={errors.password?.message?.toString()}
                                    disabled={isSubmitting}
                                >
                                    Password
                                </FormInput>
                                <FormInput
                                    id="confirmPassword"
                                    type="password"
                                    placeholder="Repeat useless password"
                                    {...register("confirmPassword")}
                                    valid={!errors.confirmPassword?.message}
                                    errorMessage={errors.confirmPassword?.message?.toString()}
                                    disabled={isSubmitting}
                                >
                                    Repeat password
                                </FormInput>
                                <motion.div className={`ml-auto mt-4`} animate={controls}>
                                    {/* <Button type="submit" colorType={color}>
                                        {hoveredTimes === 0 && isValid
                                            ? "Register"
                                            : hoveredTimes > 10
                                            ? "just fix it..."
                                            : `Or not ^ ${hoveredTimes}`}
                                    </Button> */}
                                    <Button
                                        type="submit"
                                        colorType={isValid ? "default" : "red"}
                                        loading={isSubmitting}
                                    >
                                        Register
                                    </Button>
                                </motion.div>
                            </form>

                            <small className="text-zinc-700">
                                Already registered? Sign in... for many reasons{" "}
                                <span className="text-blue-800 hover:underline">
                                    <Link href={"/auth/signin"}>here</Link>
                                </span>
                                .
                            </small>
                        </>
                    ) : (
                        <>
                            <div className="flex space-x-4">
                                <Image src={DiscordLogo} alt={"discord logo"} width={40} height={10} />
                                <Button
                                    onClick={() => signIn("discord")}
                                    loading={status === "loading"}
                                    className="ml-auto w-full"
                                    colorType="purple"
                                >
                                    Discord
                                </Button>
                            </div>
                        </>
                    )}
                </RegisterWrapper>
            </div>
            <BackButton onClick={() => router.push("/")} />
        </>
    );
}
