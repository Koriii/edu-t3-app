import { zodResolver } from "@hookform/resolvers/zod";
import { signIn, useSession } from "next-auth/react";
import Image from "next/image";
import Link from "next/link";
import router from "next/router";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { Button } from "../../../components/atoms";
import { RegisterWrapper } from "../../../components/molecules";
import BackButton from "../../../components/molecules/BackButton";
import FormInput from "../../../components/molecules/FormInput";
import { tabTypes } from "../../../components/molecules/RegisterWrapper";
import { ILogin, loginSchema } from "../../../server/common/validation/auth";
import DiscordLogo from "../../../static/discord-mark-white.svg";

export default function Login() {
    const [selectedTab, setSelectedTab] = useState<tabTypes>("credentials");
    const { status } = useSession();

    const {
        register,
        handleSubmit,
        formState: { isSubmitting, errors },
    } = useForm<ILogin>({ resolver: zodResolver(loginSchema) });

    if (Object.keys(errors).length) {
        console.log(errors);
    }

    const onSubmit = async (data: ILogin) => {
        const response = await signIn("credentials", {
            ...data,
            redirect: false,
        });
        if (response?.ok) {
            router.push("/");
        }
    };

    return (
        <>
            <div className="flex flex-col justify-center items-center h-screen">
                <RegisterWrapper selectedTab={selectedTab} setSelectedTab={(item) => setSelectedTab(item)}>
                    {selectedTab === "credentials" ? (
                        <>
                            <form className="flex flex-col" onSubmit={handleSubmit((data) => onSubmit(data))}>
                                <FormInput
                                    id="email"
                                    type="email"
                                    placeholder="useless@email.com"
                                    {...register("email")}
                                    errorMessage={"Either email"}
                                >
                                    Email
                                </FormInput>
                                <FormInput
                                    id="password"
                                    type="password"
                                    placeholder="you remember me?"
                                    {...register("password")}
                                    errorMessage={"or password is incorrect"}
                                >
                                    Password
                                </FormInput>
                                <div className={`ml-auto mt-4`}>
                                    <Button type="submit" colorType={"default"} loading={isSubmitting}>
                                        Sign in
                                    </Button>
                                </div>
                            </form>
                            <small className="text-zinc-700">
                                Not registered yet? Feel free to register{" "}
                                <span className="text-blue-800 hover:underline">
                                    <Link href={"/auth/register"}>here</Link>
                                </span>
                                .
                            </small>
                        </>
                    ) : (
                        <>
                            <div className="flex space-x-4">
                                <Image src={DiscordLogo} alt={"discord logo"} width={40} height={10} />
                                <Button
                                    onClick={() => signIn("discord")}
                                    loading={status === "loading"}
                                    className="ml-auto w-full"
                                    colorType="purple"
                                >
                                    Discord
                                </Button>
                            </div>
                        </>
                    )}
                </RegisterWrapper>
            </div>
            <BackButton onClick={() => router.push("/")} />
        </>
    );
}
