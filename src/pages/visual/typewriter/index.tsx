import router from "next/router";
import React from "react";
import BackButton from "../../../components/molecules/BackButton";
import { Typewriter } from "../../../containers/visual/typewriter";

export default function TypewriterPage() {
    return (
        <>
            <main className="p-4">
                <h2 className="text-3xl md:text-[4rem] leading-normal font-extrabold text-blue-700">
                    <Typewriter
                        initTypewriter={(builder) =>
                            builder
                                .type("Hello there captain...")
                                .pauseFor(2000)
                                .delete()
                                .type("i mean...")
                                .pauseFor(1500)
                                .delete()
                                .delete()
                                .type("General Kenobi.")
                                .start()
                        }
                    />
                </h2>
                <h3 className="text-2xl md:text-[4rem] leading-normal font-extrabold text-purple-700">
                    <Typewriter
                        initTypewriter={(builder) =>
                            builder
                                .pauseFor(12000)
                                .type("That is my little typewriter demo project i wanted to try.")
                                .pauseFor(1500)
                                .type(
                                    "It can be initialized with custom component tag <Typewriter />, and then controlled through props."
                                )
                                .pauseFor(1000)
                                .type("And it looks kinda cool.")
                                .pauseFor(1000)
                                .type("Hope you like it.")
                                .start()
                        }
                    />
                </h3>
                <h3 className="text-2xl md:text-[4rem] leading-normal font-extrabold text-purple-700">
                    <Typewriter
                        initTypewriter={(builder) =>
                            builder.pauseFor(37000).type("More options comming soon...").start()
                        }
                    />
                </h3>
            </main>
            <BackButton onClick={() => router.push("/")} />
        </>
    );
}
