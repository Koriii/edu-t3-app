import { verify } from "argon2";
import NextAuth, { type NextAuthOptions } from "next-auth";
import DiscordProvider from "next-auth/providers/discord";

// Prisma adapter for NextAuth, optional and can be removed
import { PrismaAdapter } from "@next-auth/prisma-adapter";
import Credentials from "next-auth/providers/credentials";
import { env } from "../../../env/server.mjs";
import { loginSchema } from "../../../server/common/validation/auth";
import { prisma } from "../../../server/db/client";

export const authOptions: NextAuthOptions = {
    // callbacks are not needed, email and name are passed from user to session by default
    secret: process.env.NEXTAUTH_SECRET,
    jwt: {
        maxAge: 15 * 24 * 30 * 60, // 15 days
    },
    session: {
        strategy: "jwt",
    },
    pages: {
        signIn: "/auth/signin",
        newUser: "/auth/register",
    },
    callbacks: {
        async redirect({ baseUrl }) {
            return baseUrl;
        },
    },
    // Configure one or more authentication providers
    adapter: PrismaAdapter(prisma),
    providers: [
        DiscordProvider({
            clientId: env.DISCORD_CLIENT_ID,
            clientSecret: env.DISCORD_CLIENT_SECRET,
        }),
        Credentials({
            // The name to display on the sign in form (e.g. "Sign in with...")
            name: "credentials",
            // `credentials` is used to generate a form on the sign in page.
            // You can specify which fields should be submitted, by adding keys to the `credentials` object.
            // e.g. domain, username, password, 2FA token, etc.
            // You can pass any HTML attribute to the <input> tag through the object.
            credentials: {
                email: { label: "Email", type: "email" },
                password: { label: "Password", type: "password" },
            },
            async authorize(credentials) {
                const creds = await loginSchema.parseAsync(credentials);

                const user = await prisma.user.findFirst({
                    where: { email: creds.email },
                });

                if (!user || !user.password) return null;

                const passwordMatches = await verify(user.password, creds.password);

                // If you return null then an error will be displayed advising the user to check their details.
                if (!passwordMatches) return null;
                // You can also Reject this callback with an Error thus the user will be sent to the error page with the error message as a query parameter

                // Any object returned will be saved in `user` property of the JWT
                console.log("user -> ", user);
                return user;
            },
        }),
    ],
};

export default NextAuth(authOptions);
