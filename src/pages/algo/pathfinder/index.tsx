import router from "next/router";
import { useCallback, useState } from "react";
import BackButton from "../../../components/molecules/BackButton";
import {
    astar,
    recDivisionMg,
    breadthFirstSearch,
    depthFirstSearch,
    dijkstra,
} from "../../../containers/algo/pathfinder/algos";
import Block from "../../../containers/algo/pathfinder/Block";
import PathfinderHeader from "../../../containers/algo/pathfinder/Header";
import {
    deepCloneNodes,
    doesPositionMatch,
    getMatrixProportions,
    resetMatrixVisitedNodes,
} from "../../../containers/algo/pathfinder/helpers";
import {
    algorithmType,
    clearType,
    NodeProps,
    positionType,
    setterPropsType,
} from "../../../containers/algo/pathfinder/models";

export default function Pathfinder() {
    const [isOperationInProgress, setIsOperationInProgress] = useState(false);
    const [isBoardClean, setIsBoardClean] = useState(true);

    const [removeWalls, setRemoveWalls] = useState(false);

    const [blocks, setBlocks] = useState<NodeProps[][]>(() => getMatrixProportions());

    const [startPosition, setStartPosition] = useState<positionType>([15, 5]);
    const [endPosition, setEndPosition] = useState<positionType>([15, 50]);

    const setBlockProperty = useCallback(
        (setterProp: setterPropsType, position: positionType) => {
            setBlocks((prev) => {
                const newData = deepCloneNodes(prev);
                const rows = newData.at(position[0]);
                const element = rows?.at(position[1]);
                if (element) {
                    if (setterProp === "isWall" && removeWalls) {
                        element[setterProp] = false;
                    } else {
                        element[setterProp] = true;
                    }
                }
                return newData;
            });
        },
        [removeWalls]
    );

    const triggerAction = useCallback(
        async (type: algorithmType) => {
            setIsOperationInProgress(true);
            setIsBoardClean(false);
            switch (type) {
                case "bfs":
                    await breadthFirstSearch(blocks, startPosition, endPosition, setBlockProperty);
                    break;
                case "dfs":
                    await depthFirstSearch(blocks, startPosition, endPosition, setBlockProperty);
                    break;
                case "dijkstra":
                    await dijkstra(blocks, startPosition, endPosition, setBlockProperty);
                    break;
                case "astar":
                    await astar(blocks, startPosition, endPosition, setBlockProperty);
                    break;
                case "recDivisionMg":
                    await recDivisionMg(blocks, startPosition, endPosition, setBlockProperty);
                    setIsBoardClean(true);
                    break;
                default:
                    console.error("Not existing trigger");
            }

            setIsOperationInProgress(false);
        },
        [blocks, endPosition, setBlockProperty, startPosition]
    );

    const clear = useCallback(
        (type: clearType) => {
            switch (type) {
                case "all": {
                    setBlocks(getMatrixProportions());
                    break;
                }
                case "withRandomWeights": {
                    setBlocks(getMatrixProportions(true));
                    break;
                }
                case "visited": {
                    setBlocks(resetMatrixVisitedNodes(blocks));
                    break;
                }
                default:
                    console.error("Not existing clear type");
            }
            setIsBoardClean(true);
        },
        [blocks]
    );

    const setPosition = useCallback((type: "start" | "end", position: positionType) => {
        if (type === "start") {
            setStartPosition(position);
        } else if (type === "end") {
            setEndPosition(position);
        }
    }, []);

    return (
        <>
            <PathfinderHeader
                isDisabled={isOperationInProgress}
                isBoardClean={isBoardClean}
                triggerAction={triggerAction}
                clearCallback={clear}
                setRemoveWalls={setRemoveWalls}
            />
            <div className="h-fit">
                {blocks.map((row, i) => (
                    <div key={i} className="flex">
                        {row.map((block) => (
                            <Block
                                key={`${block.position[1]}/${block.position[0]}`}
                                isWall={block.isWall}
                                visited={block.visited}
                                distance={block.distance}
                                position={block.position}
                                isPathNode={block.isPathNode}
                                isBoardClean={isBoardClean}
                                setBlockProperty={setBlockProperty}
                                isStart={doesPositionMatch(startPosition, block.position)}
                                isEnd={doesPositionMatch(endPosition, block.position)}
                                setPosition={setPosition}
                            />
                        ))}
                    </div>
                ))}
            </div>
            <small className="text-pink-900 ml-2">hold shift to set start / hold alt to set end</small>
            <BackButton onClick={() => router.push("/")} />
        </>
    );
}
