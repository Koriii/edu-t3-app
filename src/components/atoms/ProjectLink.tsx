import Link from "next/link";
import React from "react";

interface ProjectLinkProps {
    name: string;
    link: string;
}

const ProjectLink = ({ name, link }: ProjectLinkProps) => {
    return (
        <div className="relative py-3">
            <Link href={link} passHref>
                <a className="group text-white text-4xl transition-all duration-300 ease-in-out cursor-pointer">
                    <span className="bg-left-bottom rounded-sm bg-gradient-to-r from-blue-600 to-blue-600 bg-[length:0%_4px] bg-no-repeat group-hover:bg-[length:100%_4px] transition-all duration-500 ease-out">
                        {name}
                    </span>
                </a>
            </Link>
        </div>
    );
};

export default ProjectLink;
