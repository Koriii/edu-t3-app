import React, { forwardRef } from "react";
import { Spinner } from ".";

export const colorArray = ["default", "alternative", "dark", "light", "green", "red", "yellow", "purple"] as const;
export type colorType = typeof colorArray[number];

const getButtonColors = (type: colorType) => {
    switch (type) {
        case "default":
            return "text-white bg-blue-700 hover:bg-blue-800 focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800";

        case "alternative":
            return "text-gray-900 bg-white border border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-gray-200 dark:focus:ring-gray-700 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700";

        case "dark":
            return "text-white bg-gray-800 hover:bg-gray-900 focus:ring-gray-300 dark:bg-gray-800 dark:hover:bg-gray-700 dark:focus:ring-gray-700 dark:border-gray-700";

        case "light":
            return "text-gray-900 bg-white border border-gray-300 hover:bg-gray-100 focus:ring-gray-200 dark:bg-gray-800 dark:text-white dark:border-gray-600 dark:hover:bg-gray-700 dark:hover:border-gray-600 dark:focus:ring-gray-700";

        case "green":
            return "text-white bg-green-700 hover:bg-green-800 focus:ring-green-300 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800";

        case "red":
            return "text-white bg-red-700 hover:bg-red-800 focus:ring-red-300 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900";

        case "yellow":
            return "text-gray-800 bg-yellow-400 hover:bg-yellow-500 focus:ring-yellow-300 dark:focus:ring-yellow-900";

        case "purple":
            return "text-white bg-purple-700 hover:bg-purple-800 focus:ring-purple-300 dark:bg-purple-600 dark:hover:bg-purple-700 dark:focus:ring-purple-900";

        default:
            return "";
    }
};

interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
    colorType?: colorType;
    children: React.ReactNode;
    loading?: boolean;
}

const Button = forwardRef<HTMLButtonElement, ButtonProps>(
    ({ colorType = "default", children, className, loading, ...props }, ref) => {
        return (
            <>
                <button
                    ref={ref}
                    type="button"
                    className={`${getButtonColors(
                        colorType
                    )} focus:ring-4 font-medium rounded-lg text-sm focus:outline-none px-5 py-2.5 ${className}`}
                    {...props}
                >
                    {loading && <Spinner />}

                    {children}
                </button>
            </>
        );
    }
);

Button.displayName = "Button";
export default Button;
