import { signOut, useSession } from "next-auth/react";
import Link from "next/link";
import Button from "../atoms/Button";

export default function Header() {
    const { data: session, status } = useSession();

    return (
        <div className="flex align-middle items-center gap-2 py-4 pr-4">
            {status === "authenticated" ? (
                <>
                    {session && <span className="ml-auto">hello {session.user?.name ?? session.user?.email}</span>}
                    <Button
                        onClick={() => signOut({ callbackUrl: `${window.location.origin}` })}
                        className="text-xs"
                        colorType="red"
                    >
                        Log out
                    </Button>
                </>
            ) : (
                <>
                    <Link href={"/auth/signin"} passHref>
                        <Button loading={status === "loading"} className="ml-auto text-xs" colorType="default">
                            Sign in
                        </Button>
                    </Link>
                    <Link href={"/auth/register"} passHref>
                        <Button loading={status === "loading"} className="text-xs" colorType="purple">
                            Register
                        </Button>
                    </Link>
                </>
            )}
        </div>
    );
}
