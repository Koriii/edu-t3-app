import React, { forwardRef, InputHTMLAttributes, ReactNode } from "react";

interface FormInputProps extends InputHTMLAttributes<HTMLInputElement> {
    children?: ReactNode;
    label?: string;
    id: string;
    valid?: boolean;
    errorMessage?: string;
}

const errorInput =
    "bg-red-50 border border-red-500 text-red-900 placeholder-red-700 text-sm rounded-lg focus:ring-red-500 dark:bg-gray-700 focus:border-red-500 block w-full p-2.5 dark:text-red-500 dark:placeholder-red-500 dark:border-red-500";
const regularInput =
    "bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500";

const FormInput = forwardRef<HTMLInputElement, FormInputProps>(
    ({ children, label, id, valid = true, errorMessage, ...props }, ref) => {
        return (
            <div className="mt-2">
                <label htmlFor={id} className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                    {label ?? children}
                </label>
                <input id={id} ref={ref} className={valid ? regularInput : errorInput} {...props} />
                {!valid && errorMessage && (
                    <p className="mt-2 text-sm text-red-600 dark:text-red-500">{errorMessage}</p>
                )}
            </div>
        );
    }
);

FormInput.displayName = "FormInput";

export default FormInput;
