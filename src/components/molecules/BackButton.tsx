import { motion, useAnimationControls } from "framer-motion";
import React, { useRef } from "react";
import { ArrowRight } from "../atoms";

interface BackButtonProps {
    onClick: () => void;
}

const gray500 = "rgb(107 114 128)";

export default function BackButton({ onClick }: BackButtonProps) {
    const controls = useAnimationControls();
    const constraintsRef = useRef(null);

    return (
        <div className="fixed -left-5 -bottom-5 rotate-180" ref={constraintsRef} onClick={onClick}>
            <motion.div
                className="flex items-center justify-center rounded-full w-[100px] h-[100px] hover:cursor-pointer"
                animate={{ ...controls }}
                initial={{ backgroundColor: gray500, opacity: 0.5 }}
                whileHover={{
                    scale: 2,
                    opacity: 0.9,
                    transition: { duration: 1 },
                    backgroundColor: "rgb(229 231 235)", // gray-200
                }}
                onHoverEnd={() =>
                    controls.start({
                        backgroundColor: gray500,
                        scale: [2, 0.3, 1, 0.5, 1],
                        opacity: 0.5,
                    })
                }
                whileTap={{ scale: 1.6 }}
                // drag
                // dragConstraints={constraintsRef}
            >
                <ArrowRight fill="black" />
            </motion.div>
        </div>
    );
}
