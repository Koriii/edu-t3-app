import { AnimatePresence, motion } from "framer-motion";
import React from "react";

const tabs = ["credentials", "providers"] as const;
export type tabTypes = typeof tabs[number];

interface RegisterWrapperProps {
    children: React.ReactNode;
    selectedTab: tabTypes;
    setSelectedTab: (item: tabTypes) => void;
}

export default function RegisterWrapper({ children, selectedTab, setSelectedTab }: RegisterWrapperProps) {
    return (
        <div className="flex flex-col w-96 h-96">
            <nav className="border-b-2 text-center border-b-gray-800">
                <ul className="flex">
                    {tabs.map((item) => (
                        <li
                            key={item}
                            className={`relative w-1/2 px-4 py-2 cursor-pointer content-between rounded-t-md hover:bg-zinc-900`}
                            onClick={() => setSelectedTab(item)}
                        >
                            {`${item}`}
                            {item === selectedTab ? (
                                <motion.div
                                    className="absolute bg-gray-300 -bottom-[1px] left-0 right-0 h-[1px]"
                                    layoutId="underline"
                                />
                            ) : null}
                        </li>
                    ))}
                </ul>
            </nav>
            <main className="p-4">
                <AnimatePresence exitBeforeEnter>
                    <motion.div
                        key={selectedTab ?? "empty"}
                        initial={{ y: 10, opacity: 0 }}
                        animate={{ y: 0, opacity: 1 }}
                        exit={{ y: -10, opacity: 0 }}
                        transition={{ duration: 0.2 }}
                    >
                        {children}
                    </motion.div>
                </AnimatePresence>
            </main>
        </div>
    );
}
