export type positionType = [number, number];
export type algorithmType = "bfs" | "dfs" | "dijkstra" | "astar" | "recDivisionMg";
export type setterPropsType = "visited" | "isWall" | "isPathNode";
export type clearType = "all" | "visited" | "withRandomWeights";

export interface NodeProps {
    position: positionType;
    visited: boolean;
    isWall: boolean;
    isPathNode: boolean;
    parent?: positionType;
    distance?: number;
}

export const initNodeProps: NodeProps = {
    position: [0, 0],
    visited: false,
    isWall: false,
    isPathNode: false,
};
