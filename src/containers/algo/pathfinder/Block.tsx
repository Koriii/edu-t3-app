import { motion, useAnimationControls } from "framer-motion";
import { memo } from "react";
import { positionType, setterPropsType } from "./models";

interface BlockProps {
    setBlockProperty: (setterProp: setterPropsType, position: positionType) => void;
    setPosition: (type: "start" | "end", position: positionType) => void;
    position: positionType;
    isWall: boolean;
    visited: boolean;
    isStart: boolean;
    isEnd: boolean;
    isPathNode: boolean;
    isBoardClean: boolean;
    distance?: number;
}

const Block = memo(
    ({
        setBlockProperty,
        isStart,
        isEnd,
        isWall,
        visited,
        isPathNode,
        isBoardClean,
        position,
        setPosition,
    }: // distance,
    BlockProps) => {
        const controls = useAnimationControls();

        const getBackgroundColor = () => {
            if (isWall) return "rgb(107, 114, 128)"; // gray-700
            if (isStart) return "rgb(59, 130, 246)"; // blue-500
            if (isEnd) return "rgb(239, 68, 68)"; // red-500
            if (isPathNode) return `rgb(134, 239, 172)`; // green-300
            if (visited) return `rgb(234, 179, 8)`; // yellow-500
            return "rgb(0, 0, 0)";
        };

        const onClick = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
            e.preventDefault();
            if (isBoardClean) {
                if (!isEnd && e.shiftKey) setPosition("start", position);
                else if (!isStart && e.altKey) setPosition("end", position);
                else if (!isStart && !isEnd) setBlockProperty("isWall", position);
            }
        };

        if (isWall) {
            controls.start({ backgroundColor: ["rgb(209, 213, 219)", "rgb(107, 114, 128)"] });
        } else if (isStart) {
            controls.start({ backgroundColor: "rgb(59, 130, 246)" });
        } else if (isEnd) {
            controls.start({ backgroundColor: "rgb(239, 68, 68)" });
        } else if (isPathNode) {
            controls.start({ backgroundColor: ["rgb(217 249 157)", "rgb(134, 239, 172)"] });
        } else if (visited) {
            controls.start({
                animationFillMode: "revert-layer",
                backgroundSize: ["0", "100%"],
                backgroundColor: ["rgb(0, 0, 0)", "rgb(99 102 241)", "rgb(234, 179, 8)"],
                transition: { duration: 2 },
            });
        } else {
            controls.start({ backgroundColor: "rgb(0, 0, 0)" });
        }

        return (
            <motion.div
                initial={{ backgroundColor: getBackgroundColor() }}
                animate={controls}
                className={`border w-full aspect-square`}
                onClick={onClick}
                onMouseOver={(e) => {
                    e.preventDefault();
                    e.buttons === 1 && !isStart && !isEnd && isBoardClean && setBlockProperty("isWall", position);
                }}
            >
                {/* <div className="absolute"> */}
                {/* {position[0]}/{position[1]} */}
                {/* </div> */}
                {/* {distance} */}
            </motion.div>
        );
    }
);

Block.displayName = "Block";

export default Block;
