import { Dispatch, memo, SetStateAction } from "react";
import { Button, Checkbox } from "../../../components/atoms";
import { algorithmType, clearType } from "./models";

interface PathfinderHeaderProps {
    isDisabled: boolean;
    isBoardClean: boolean;
    clearCallback: (type: clearType) => void;
    triggerAction: (type: algorithmType) => void;
    setRemoveWalls: Dispatch<SetStateAction<boolean>>;
}

const PathfinderHeader = memo(
    ({ triggerAction, clearCallback, isDisabled, isBoardClean, setRemoveWalls }: PathfinderHeaderProps) => {
        return (
            <section className="flex flex-wrap mb-4 gap-2">
                <Button onClick={() => clearCallback("all")} disabled={isDisabled} colorType="default">
                    clear all
                </Button>
                <Button onClick={() => clearCallback("withRandomWeights")} disabled={isDisabled} colorType="default">
                    clear all with random node weights
                </Button>
                <Button onClick={() => clearCallback("visited")} disabled={isDisabled} colorType="default">
                    clear visited
                </Button>
                <Button onClick={() => triggerAction("bfs")} disabled={isDisabled || !isBoardClean} colorType="red">
                    bfs
                </Button>
                <Button onClick={() => triggerAction("dfs")} disabled={isDisabled || !isBoardClean} colorType="red">
                    dfs
                </Button>
                <Button
                    onClick={() => triggerAction("dijkstra")}
                    disabled={isDisabled || !isBoardClean}
                    colorType="red"
                >
                    dijkstra
                </Button>
                <Button onClick={() => triggerAction("astar")} disabled={isDisabled || !isBoardClean} colorType="red">
                    A*
                </Button>
                <Button
                    onClick={() => triggerAction("recDivisionMg")}
                    disabled={isDisabled || !isBoardClean}
                    colorType="green"
                >
                    recursive division
                </Button>
                <Checkbox onChange={(e) => setRemoveWalls(e.target.checked)} label="Remove walls" />
                {/* <div className="flex items-center">
                    <input
                        id="default-checkbox"
                        type="checkbox"
                        className="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                        onChange={(e) => setRemoveWalls(e.target.checked)}
                    />
                    <label
                        htmlFor="default-checkbox"
                        className="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300"
                    ></label>
                </div> */}
            </section>
        );
    }
);

PathfinderHeader.displayName = "PathfinderHeader";

export default PathfinderHeader;
