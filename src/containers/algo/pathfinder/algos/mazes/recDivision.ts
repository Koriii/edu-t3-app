import { deepCloneNodes, delay, doesPositionMatch, gridHeight, gridWidth } from "../../helpers";
import { NodeProps, positionType, setterPropsType } from "../../models";

type orientationType = "horizontal" | "vertical";

const getRandomInt = (max: number) => Math.round(Math.random() * max);

const getRandomArbitrary = (min: number, max: number) => {
    return Math.round(Math.random() * (max - min) + min);
};

const chooseOrientation = (width: number, height: number) => {
    if (width < height) {
        return "horizontal";
    } else if (width > height) {
        return "vertical";
    } else {
        return getRandomInt(2) == 0 ? "horizontal" : "vertical";
    }
};

export const recDivisionMg = async (
    matrix: NodeProps[][],
    start: positionType,
    end: positionType,
    setBlockProperty: (setterProp: setterPropsType, position: positionType) => void,
    skew: orientationType = "horizontal"
) => {
    const isValidWall = (position: positionType) =>
        !doesPositionMatch(position, start) && !doesPositionMatch(position, end);

    const cloneMatrix = deepCloneNodes(matrix) as NodeProps[][];

    const divide = async (y: number, x: number, height: number, width: number) => {
        let orientation = skew;
        let new_wall = 0;
        let new_hole = 0;
        let new_height = 0;
        let new_width = 0;
        let y_pair = 0;
        let x_pair = 0;
        let new_height_pair = 0;
        let new_width_pair = 0;

        /* Decide which orientation the wall should be. */
        /* If one of the is longer, then use the opposite. */
        /* If they are the same length, then use a random orientation. */
        orientation = chooseOrientation(width, height);

        if (orientation == "horizontal") {
            /* Not enough space, stop. */
            if (height < 5) return;

            /* Random place for the wall and for the hole. */
            const randomWall = getRandomArbitrary(2, height - 3);
            const randomHole = getRandomArbitrary(1, width - 2);

            /* Make sure, that the wall is on an even coordinate and the hole is on an odd coordinate. */
            new_wall = y + Math.round(randomWall / 2) * 2;
            new_hole = x + (Math.round(randomHole / 2) * 2 - 1);

            /* Place the wall. */
            for (let i = x; i < x + width - 1; i++) {
                if (i !== new_hole) {
                    const wallNode = cloneMatrix[new_wall]?.[i];
                    if (wallNode && isValidWall(wallNode.position)) {
                        wallNode.isWall = true;
                        setBlockProperty("isWall", wallNode.position);
                        await delay();
                    }
                }
            }

            /* Calculate the new values for the next run. */
            new_height = new_wall - y + 1;
            new_width = width;
            /* Complementary pairs. 'The other side of the wall.' */
            y_pair = new_wall;
            x_pair = x;
            new_height_pair = y + height - new_wall;
            new_width_pair = width;
        } else if (orientation == "vertical") {
            /* Not enough space, stop. */
            if (width < 5) return;

            /* Random place for the wall and for the hole. */
            const randomWall = getRandomArbitrary(2, width - 3);
            const randomHole = getRandomArbitrary(1, height - 2);

            /* Make sure, that the wall is on an even coordinate and the hole is on an odd coordinate. */
            new_wall = x + Math.round(randomWall / 2) * 2;
            new_hole = y + (Math.round(randomHole / 2) * 2 - 1);

            /* Place the wall. */
            for (let i = y; i < y + height - 1; i++) {
                if (i !== new_hole) {
                    const wallNode = cloneMatrix[i]?.[new_wall];
                    if (wallNode && isValidWall(wallNode.position)) {
                        wallNode.isWall = true;
                        setBlockProperty("isWall", wallNode.position);
                        await delay();
                    }
                }
            }

            /* Calculate the new values for the next run. */
            new_height = height;
            new_width = new_wall - x + 1;
            /* Complementary pairs. 'The other side of the wall.' */
            y_pair = y;
            x_pair = new_wall;
            new_height_pair = height;
            new_width_pair = x + width - new_wall;
        }

        /* Call it again. */
        await divide(y, x, new_height, new_width);
        /* When there are no more places left, then go to the 'other side'. */
        await divide(y_pair, x_pair, new_height_pair, new_width_pair);
    };

    await divide(0, 0, gridHeight + 1, gridWidth + 1);
};
