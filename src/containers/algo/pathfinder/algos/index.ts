import { astar } from "./finder/astar";
import { breadthFirstSearch } from "./finder/breadthFirstSearch";
import { depthFirstSearch } from "./finder/depthFirstSearch";
import { dijkstra } from "./finder/dijkstra";
import { recDivisionMg } from "./mazes/recDivision";

export { depthFirstSearch, breadthFirstSearch, dijkstra, astar, recDivisionMg };
