import { backtrace, deepCloneNodes, delay, direction, doesPositionMatch, gridHeight, gridWidth } from "../../helpers";
import { NodeProps, positionType, setterPropsType } from "../../models";

const getClosestDistance = (matrixMap: Map<positionType, number>) => {
    const closestNodeDistance = { position: [0, 0] as positionType, distance: Infinity };
    matrixMap.forEach((value, key) => {
        if (value < closestNodeDistance.distance) {
            closestNodeDistance.distance = value;
            closestNodeDistance.position = key;
        }
    });
    return closestNodeDistance.position;
};

export const dijkstra = async (
    matrix: NodeProps[][],
    start: positionType,
    end: positionType,
    setBlockProperty: (setterProp: setterPropsType, position: positionType) => void
) => {
    const cloneMatrix = deepCloneNodes(matrix);
    const queueMap = new Map<positionType, number>();

    const startNode = cloneMatrix.at(start[0])?.at(start[1]);
    if (startNode) {
        queueMap.set(startNode.position, 0);
        startNode.visited = true;
    }

    while (queueMap.size > 0) {
        const currentPosition = getClosestDistance(queueMap);
        const currentNode = cloneMatrix[currentPosition[0]]?.[currentPosition[1]];

        if (doesPositionMatch(end, currentPosition))
            return await backtrace(cloneMatrix, currentPosition, start, setBlockProperty);

        if (currentNode) {
            queueMap.delete(currentPosition);
            currentNode.visited = true;
            setBlockProperty("visited", currentNode.position);
        }

        for (let i = 0; i < direction.length; i++) {
            const a = currentPosition[0] + (direction[i]?.[0] ?? 0);
            const b = currentPosition[1] + (direction[i]?.[1] ?? 0);
            const neighbour = cloneMatrix[a]?.[b];

            if (
                a >= 0 &&
                b >= 0 &&
                a < gridHeight &&
                b < gridWidth &&
                neighbour &&
                !neighbour.isWall &&
                !neighbour.visited
            ) {
                neighbour.parent = currentPosition;
                queueMap.set(neighbour.position, neighbour.distance ?? 0);
            }
        }
        await delay();
    }
};
