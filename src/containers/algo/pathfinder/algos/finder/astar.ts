import { backtrace, deepCloneNodes, delay, direction, doesPositionMatch, gridHeight, gridWidth } from "../../helpers";
import { NodeProps, positionType, setterPropsType } from "../../models";

const getClosestDistance = (matrixMap: Map<positionType, MapValueObject>) => {
    const closestNodeDistance = { position: [0, 0] as positionType, totalDistance: Infinity };
    matrixMap.forEach((value, key) => {
        const summedValue = value.distance + value.manhattanDistance;
        if (summedValue < closestNodeDistance.totalDistance) {
            closestNodeDistance.totalDistance = summedValue;
            closestNodeDistance.position = key;
        }
    });
    return closestNodeDistance.position;
};

const getManhattanDistance = (end: positionType, node: positionType) =>
    Math.abs(end[0] - node[0]) + Math.abs(end[1] - node[1]);

interface MapValueObject {
    distance: number;
    manhattanDistance: number;
}

export const astar = async (
    matrix: NodeProps[][],
    start: positionType,
    end: positionType,
    setBlockProperty: (setterProp: setterPropsType, position: positionType) => void
) => {
    const cloneMatrix = deepCloneNodes(matrix);
    const queueMap = new Map<positionType, MapValueObject>();

    const startNode = cloneMatrix.at(start[0])?.at(start[1]);
    if (startNode) {
        queueMap.set(startNode.position, { distance: 0, manhattanDistance: getManhattanDistance(end, start) });
        startNode.visited = true;
    }

    while (queueMap.size > 0) {
        const currentPosition = getClosestDistance(queueMap);
        const currentNode = cloneMatrix[currentPosition[0]]?.[currentPosition[1]];

        if (doesPositionMatch(end, currentPosition)) {
            return await backtrace(cloneMatrix, currentPosition, start, setBlockProperty);
        }

        if (currentNode) {
            queueMap.delete(currentPosition);
            currentNode.visited = true;
            setBlockProperty("visited", currentNode.position);
        }

        for (let i = 0; i < direction.length; i++) {
            const a = currentPosition[0] + (direction[i]?.[0] ?? 0);
            const b = currentPosition[1] + (direction[i]?.[1] ?? 0);
            const neighbour = cloneMatrix[a]?.[b];

            if (
                a >= 0 &&
                b >= 0 &&
                a < gridHeight &&
                b < gridWidth &&
                neighbour &&
                !neighbour.isWall &&
                !neighbour.visited
            ) {
                neighbour.parent = currentPosition;
                queueMap.set(neighbour.position, {
                    distance: (neighbour.distance ?? 0) + getManhattanDistance(start, neighbour.position),
                    manhattanDistance: getManhattanDistance(end, neighbour.position),
                });
            }
        }
        await delay();
    }
};
