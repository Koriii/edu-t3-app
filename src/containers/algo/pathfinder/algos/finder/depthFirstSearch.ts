import { NodeProps, positionType, setterPropsType } from "../../models";
import { backtrace, deepCloneNodes, delay, direction, doesPositionMatch, gridHeight, gridWidth } from "../../helpers";

export const depthFirstSearch = async (
    matrix: NodeProps[][],
    start: positionType,
    end: positionType,
    setBlockProperty: (setterProp: setterPropsType, position: positionType) => void
) => {
    const cloneMatrix = deepCloneNodes(matrix);
    const queue = [];

    const startNode = cloneMatrix.at(start[0])?.at(start[1]);
    if (startNode) {
        startNode.visited = true;
        queue.push(startNode.position);
    }

    while (Boolean(queue.length > 0)) {
        const currentPosition = queue.pop() as positionType;
        const currentNode = cloneMatrix[currentPosition[0]]?.[currentPosition[1]];

        if (doesPositionMatch(end, currentPosition))
            return await backtrace(cloneMatrix, currentPosition, start, setBlockProperty);

        if (currentNode) {
            currentNode.visited = true;
            setBlockProperty("visited", currentNode.position);
        }

        for (let i = 0; i < direction.length; i++) {
            const a = currentPosition[0] + (direction[i]?.[0] ?? 0);
            const b = currentPosition[1] + (direction[i]?.[1] ?? 0);
            const neighbour = cloneMatrix[a]?.[b];

            if (
                a >= 0 &&
                b >= 0 &&
                a < gridHeight &&
                b < gridWidth &&
                neighbour &&
                !neighbour.isWall &&
                !neighbour.visited
            ) {
                neighbour.parent = currentPosition;
                queue.push(neighbour.position);
            }
        }
        await delay();
    }
};
