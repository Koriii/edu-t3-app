import { NodeProps, initNodeProps, positionType, setterPropsType } from "./models";

export const direction = [
    [0, 1],
    [1, 0],
    [0, -1],
    [-1, 0],
];
export const gridHeight = 30;
export const gridWidth = 60;

export const doesPositionMatch = (pos1: positionType, pos2: positionType) => pos1[0] === pos2[0] && pos1[1] === pos2[1];

export const deepCloneNodes = (blocks: NodeProps[][]) => {
    const result = [];
    for (const rows of blocks) {
        const row = [];
        for (const element of rows) {
            row.push({ ...element });
        }
        result.push(row);
    }
    return result;
};

export const getMatrixProportions = (randomizeDistances = false) => {
    const rows = [];
    for (let i = 0; i < gridHeight; i++) {
        const nodes = [];
        for (let j = 0; j < gridWidth; j++) {
            const randomNumber = Math.floor(Math.random() * 10) + 1;
            nodes.push({
                ...initNodeProps,
                position: [i, j] as positionType,
                distance: randomizeDistances ? randomNumber : undefined,
            });
        }
        rows.push(nodes);
    }

    return rows;
};

export const resetMatrixVisitedNodes = (matrix: NodeProps[][]) => {
    const rows = [] as NodeProps[][];
    for (let i = 0; i < matrix.length; i++) {
        const nodes = [] as NodeProps[];
        const row = matrix[i];
        if (row) {
            for (let j = 0; j < row.length; j++) {
                const node = row[j];
                if (node) {
                    nodes.push({
                        ...node,
                        visited: false,
                        isPathNode: false,
                        parent: undefined,
                    });
                }
            }
        }
        rows.push(nodes);
    }

    return rows;
};

export const delay = (time = 10) => {
    return new Promise((resolve) => setTimeout(resolve, time));
};

export const backtrace = async (
    matrix: NodeProps[][],
    currentPosition: positionType,
    start: positionType,
    setBlockProperty: (setterProp: setterPropsType, position: positionType) => void
) => {
    await delay(600);
    const findParent = async (node: NodeProps) => {
        if (node.parent) {
            const parentNode = matrix[node.parent[0]]?.[node.parent[1]];
            if (parentNode && !doesPositionMatch(start, parentNode.position)) {
                await delay();
                setBlockProperty("isPathNode", parentNode.position);
                findParent(parentNode);
            }
        }
    };

    if (matrix) {
        const node = matrix[currentPosition[0]]?.[currentPosition[1]];
        if (node && !doesPositionMatch(start, node.position)) {
            await delay();
            setBlockProperty("isPathNode", node.position);
            findParent(node);
        }
    }
};
