import { motion } from "framer-motion";
import React, { useCallback, useEffect, useRef, useState } from "react";

interface TypewriterProps {
    options?: {
        strings: string[];
        delay?: number;
        delayBetweenWords?: number;
        loop?: boolean;
        typedDelete?: boolean;
    };
    initTypewriter?: (builder: TypewriterBuilder) => TypewriterQueue[];
}

const delay = (time = 100) => {
    return new Promise((resolve) => setTimeout(resolve, time));
};

interface TypewriterQueue {
    type: "type" | "pause" | "delete" | "deleteAll" | "start";
    value?: string | number;
}

export const Typewriter = ({ options, initTypewriter }: TypewriterProps) => {
    const [text, setText] = useState<string>("");
    const textRef = useRef("");
    const textWrapperRef = useRef<HTMLSpanElement>(null);
    const triggeredRef = useRef(false);

    const processQueue = useCallback(async (actionQueue: TypewriterQueue[], defaultDelay?: number) => {
        while (actionQueue.length) {
            const action = actionQueue.shift();
            if (!action) break;
            switch (action.type) {
                case "type":
                    if (typeof action.value === "string") {
                        for (const s of action.value) {
                            setText((prev) => {
                                const value = [...Array.from(prev), s].join("");
                                textRef.current = value;
                                return value;
                            });

                            await delay(defaultDelay);
                        }
                    }
                    break;
                case "pause":
                    if (typeof action.value === "number") {
                        await delay(action.value);
                    }
                    break;
                case "delete":
                    for (let i = 0; i < (action.value ?? 1); i++) {
                        const indexOfLastWord = textRef.current.lastIndexOf(" ");
                        for (let i = textRef.current.length - 1; i >= indexOfLastWord; i--) {
                            setText((prev) => {
                                const value = prev.substring(0, i);
                                textRef.current = value;
                                return value;
                            });
                            await delay(defaultDelay);
                        }
                    }
                    break;
                case "deleteAll":
                    for (let i = textRef.current.length - 1; i >= 0; i--) {
                        setText((prev) => {
                            const value = prev.substring(0, i);
                            textRef.current = value;
                            return value;
                        });
                        await delay(defaultDelay);
                    }
                    break;
                default:
                    console.error("unexpected type");
            }
        }
    }, []);

    const processOptions = useCallback(async () => {
        if (options) {
            await delay(1000);
            for (const string of options.strings) {
                setText((prev) => [...Array.from(prev), " "].join(""));
                for (const s of string) {
                    setText((prev) => {
                        const value = [...Array.from(prev), s].join("");
                        textRef.current = value;
                        return value;
                    });
                    await delay(options.delay);
                }

                if (options.delayBetweenWords) {
                    await delay(options.delayBetweenWords);
                }
            }

            if (options.typedDelete) {
                await delay(1000);
                for (let i = textRef.current.length - 1; i >= 0; i--) {
                    setText((prev) => prev.substring(0, i));
                    await delay(options.delay);
                }
            }

            if (options.loop) {
                await delay(1000);
                setText("");
                processOptions();
            }
        }
    }, [options]);

    useEffect(() => {
        if (!triggeredRef.current) {
            triggeredRef.current = true;
            if (options) {
                processOptions();
            } else if (initTypewriter) {
                const commands = initTypewriter(new TypewriterBuilder());
                processQueue(commands);
            }
        }
    }, [initTypewriter, options, processOptions, processQueue]);

    return (
        <>
            <span ref={textWrapperRef}>{text}</span>
            {text && (
                <motion.span animate={{ opacity: [1, 0, 1] }} transition={{ duration: 1, repeat: Infinity }}>
                    <span className="relative tracking-tight">|</span>
                </motion.span>
            )}
        </>
    );
};

class TypewriterBuilder {
    private actionQueue: TypewriterQueue[];
    public defaultDelay?: number;

    constructor(defaultDelay?: number) {
        this.actionQueue = [];
        this.defaultDelay = defaultDelay;
    }

    type(text: string) {
        this.actionQueue.push({ type: "type", value: " " + text });
        return this;
    }

    pauseFor(delay: number) {
        this.actionQueue.push({ type: "pause", value: delay });
        return this;
    }

    delete(numberOfWords?: number) {
        this.actionQueue.push({ type: "delete", value: numberOfWords });
        return this;
    }

    deleteAll() {
        this.actionQueue.push({ type: "deleteAll" });
        return this;
    }

    start() {
        return this.actionQueue;
    }
}
