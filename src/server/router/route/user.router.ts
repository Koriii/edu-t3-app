import * as trpc from "@trpc/server";
import { hash } from "argon2";
import { createUserSchema } from "../../common/validation/auth";
import { createRouter } from "../context";

export const userRouter = createRouter().mutation("register-user", {
    input: createUserSchema,
    resolve: async ({ input, ctx }) => {
        const { email, password } = input;

        const exists = await ctx.prisma.user.findFirst({ where: { email } });

        if (exists) {
            throw new trpc.TRPCError({ code: "CONFLICT", message: "User already exists" });
        }

        const hashedPassword = await hash(password);

        try {
            const result = await ctx.prisma.user.create({
                data: { email, password: hashedPassword },
            });

            return {
                status: 201,
                message: "Account created successfully",
                result: result.email,
            };
        } catch {
            throw new trpc.TRPCError({ code: "INTERNAL_SERVER_ERROR", message: "Something went terribly wrong" });
        }
    },
});
