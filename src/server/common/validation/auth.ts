// @/src/server/common/validation/auth.ts
import * as z from "zod";

export const loginSchema = z.object({
    email: z.string().email(),
    password: z.string().min(4).max(12),
});

export const createUserSchema = loginSchema
    .extend({
        confirmPassword: z.string(),
    })
    .refine((data) => data.password === data.confirmPassword, {
        message: "Passwords don't match",
        path: ["confirmPassword"], // path of error
    });

export type ILogin = z.infer<typeof loginSchema>;
export type ICreateUser = z.infer<typeof createUserSchema>;
